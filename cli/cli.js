#!/usr/bin/env node
const minimist = require('minimist');
const CommandExecutor = require('./execution/commandExecutor');
const xml2js = require('xml2js');
const fs = require('fs').promises;
const moment = require('moment');
const Map = require('./modules/map');
const map = new Map(xml2js, fs, moment);
const executor = new CommandExecutor(map);

const main = async (args) => {
    try {
        const args = minimist(process.argv.slice(2), {
            alias: {
              'o': 'output',
            }
        });
        await executor.executeCommand(args);
    } catch(err) {
        console.error(err);
        console.error(`Oh no! This sucks. We encountered an unexpected error. 
Please contact Obsidian QA support @ support@obsidianqa.com for help, we'll make this right!`);
    }
}

const sleep = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}

// Drop into the entry point...
(async () => {
    const [, , ...args] = process.argv;
    await main(args);
})();