class Map {
    xml2js
    fs
    moment
    requiredArgs = [
      'output'
    ]
  
    constructor(xml2js, fs, moment) {
      this.xml2js = xml2js;
      this.fs = fs;
      this.moment = moment;
      this.timestamps = [];
    }
  
     getTestCases(testRun) {
      let cases = [];

      let testFile = '';

      for (let suite of testRun.testsuite) {
        if (suite['$'].name === 'Root Suite') {
          testFile = suite['$'].file;
        }

        if(parseInt(suite['$'].tests) > 0) {
          cases = cases.concat(this.recurseSuites(suite, testFile));
        }
      }

      return cases;
    }
  
    recurseSuites(suite, testFile) {
      if(!suite.testsuite) {
          let timestamp = suite['$'].timestamp;
          if (timestamp && !timestamp.endsWith('Z')) {
            timestamp += 'Z';
          }

          this.timestamps.push(timestamp);
          let cases = suite.testcase.filter(t => t.skipped == null);
          return cases.map((testCase) => {
            return {
              testFile,
              ...testCase,
            }
          });
      }

      let cases = []
      suite.testsuite.forEach(suite => {
        const timestamp = suite['$'].timestamp;
        this.timestamps.push(timestamp);
        cases = cases.concat(this.recurseSuites(suite));
      });

      return cases;
    }

    getStartTime() {
      let moments = this.timestamps.map(t => this.moment(t));
      return this.moment.min(moments);
    }
    getEndTime() {
      let moments = this.timestamps.map(t => this.moment(t));
      return this.moment.max(moments);
    }
  
    getMessage(testCase) {
      if(testCase.failure && testCase.failure[0]['$'].message) {
        return testCase.failure[0]['$'].message.replace('\n', ',').replace(/\s+/g,' ').trim();
      } else if (testCase.error && testCase.error[0]['$'].message) {
        return testCase.error[0]['$'].message.replace('\n', ',').replace(/\s+/g,' ').trim();
      }
      return '';
    }

    getStackTrace(testCase) {
      if(testCase.failure) {
        return testCase.failure[0]['_'];
      } else if (testCase.error) {
        return testCase.error[0]['_'];
      }
      return '';
    }

    async handleCommand(args) {
      if (args['_'].length < 2) {
        throw new Error(`You must provide a file to map! See obsidian-junit-mapper map --help.`)
      }
  
      for (let i = 0; i < this.requiredArgs.length; i++) {
        if(!args[this.requiredArgs[i]]) {
          throw new Error(`Required argument ${this.requiredArgs[i]} not supplied. See obsidian-junit-mapper map --help.`)
        }
      }
  
      try {
        const inputFilename = args['_'][1];
        const inputFileContent = await this.fs.readFile(inputFilename);
        const result = await this.xml2js.parseStringPromise(inputFileContent);
        
        let testSuitesJson = null; 
        if(result.testsuites) {
          testSuitesJson = result.testsuites;
        } else if(result.testsuite) {
          testSuitesJson = {testsuite: [result.testsuite]};
        } else {
          throw new Error(`Failed to find any testsuite elements.`)
        }
        
        let testRun = {};
        const testCases = this.getTestCases(testSuitesJson);
  
        let executions = [];
        testCases.forEach(testCase => {
          executions.push({
            message: this.getMessage(testCase),
            succeeded: !testCase.failure && !testCase.error,
            testName: testCase['$'].name,
            stackTrace: this.getStackTrace(testCase),
            duration:  testCase['$'].time ? testCase['$'].time * 1000 : null,
            testFile: testCase.testFile,
          });
        });
  
        testRun.success = executions.every(exec => exec.succeeded);
        testRun.passes = executions.filter(exec => exec.succeeded).length;
        testRun.failures = executions.filter(exec => !exec.succeeded).length;
        testRun.executions = executions;

        if(this.timestamps.length === 0)
        {
          return this.timestamps.push(this.moment());
        }
        testRun.startTimeUtc = this.getStartTime();
        testRun.endTimeUtc = this.getEndTime();

        await this.fs.writeFile(args.output, JSON.stringify(testRun));
      } catch(err) {
        if (args.verbose) {
          console.error(err);
        }
  
        throw new Error(`Encountered an error while reading and mapping your input JUnit.`);
      }
    }
  }
  
  module.exports = Map;
